# CHANGELOG.md

### v0.0.3 (Feb 2021)
* Updated metadata and module name -- [Devin Sylva](dsylva@gitlab.com)

### v0.0.1 (Feb 2021)
 * Original version of the gitlab-teleport -- [Devin Sylva](dsylva@gitlab.com)

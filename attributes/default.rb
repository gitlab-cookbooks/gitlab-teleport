#
# Cookbook:: GitLab::teleport
# Attributes:: gitlab-teleport
#

default['gitlab-teleport']['version'] = '16.1.7'
default['gitlab-teleport']['checksum'] = 'fe1833aead26dfc8c6b4df4eb15d4acf234cb93cafc8bb4e134f4682a9bb0264'
default['gitlab-teleport']['package_url'] = "https://cdn.teleport.dev/teleport-ent_#{default['gitlab-teleport']['version']}_amd64.deb"

default['gitlab-teleport']['enabled'] = true
default['gitlab-teleport']['config_dir'] = '/etc/teleport'

default['gitlab-teleport']['secrets']['backend'] = 'hashicorp_vault'
default['gitlab-teleport']['secrets']['path']['mount'] = 'chef'
default['gitlab-teleport']['secrets']['path']['path'] = 'env/teleport-production/cookbook/gitlab-teleport/agent'
default['gitlab-teleport']['secrets']['key'] = ''

default['gitlab-teleport']['teleport_addr'] = 'internal.production.teleport.gitlab.net:443'
default['gitlab-teleport']['log_severity'] = 'INFO'
default['gitlab-teleport']['enhanced_recording'] = false

default['gitlab-teleport']['environment'] = ''
default['gitlab-teleport']['service'] = ''

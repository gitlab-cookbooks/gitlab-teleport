#
# Cookbook:: gitlab-teleport
# Recipe:: default
#
# Copyright:: (C) 2022 GitLab Inc.
#
# License: MIT
#

teleport_deb_file = File.join(Chef::Config[:file_cache_path], "teleport-ent_#{node['gitlab-teleport']['version']}_amd64.deb")
teleport_config = "#{node['gitlab-teleport']['config_dir']}/config.yaml"

secrets = get_secrets(
  node['gitlab-teleport']['secrets']['backend'],
  node['gitlab-teleport']['secrets']['path'],
  node['gitlab-teleport']['secrets']['key']
)

# Download Teleport Debian package
remote_file teleport_deb_file do
  source node['gitlab-teleport']['package_url']
  mode '0644'
  checksum node['gitlab-teleport']['checksum']
end

# Upgrade Teleport Debian package
dpkg_package 'gitlab-teleport' do
  source teleport_deb_file
  action :upgrade
  notifies :restart, 'service[teleport]'
end

# Create the top-level directory for Teleport configurations
directory node['gitlab-teleport']['config_dir'] do
  owner node['gitlab-teleport']['user']
  group node['gitlab-teleport']['group']
  mode '0755'
  recursive true
end

# Create the directory for Teleport SSL configurations
directory "#{node['gitlab-teleport']['config_dir']}/ssl" do
  owner node['gitlab-teleport']['user']
  group node['gitlab-teleport']['group']
  mode '0755'
  recursive true
end

# Create Teleport configuration file
template teleport_config do
  source 'node-teleport.yaml.erb'
  path teleport_config
  owner node['gitlab-teleport']['user']
  group node['gitlab-teleport']['group']
  mode '0600'
  variables(
    teleport_addr: node['gitlab-teleport']['teleport_addr'],
    token_name: secrets['tokens']["#{Chef::Config[:node_name]}"],
    ca_pin: secrets['ca_pin'],
    node_name: Chef::Config[:node_name],
    log_severity: node['gitlab-teleport']['log_severity'],
    enhanced_recording: node['gitlab-teleport']['enhanced_recording'],
    environment: node['gitlab-teleport']['environment'],
    service: node['gitlab-teleport']['service']
  )
  notifies :restart, 'service[teleport]'
end

# Set up a systemd service for Teleport agent
systemd_unit 'teleport.service' do
  content(
    Unit: {
      Description: 'Teleport: Identity-Native Infrastructure Access',
      Documentation: 'https://goteleport.com/docs/',
      Requires: 'network-online.target',
      After: 'network-online.target',
      ConditionFileNotEmpty: teleport_config,
      StartLimitBurst: '3'
    },
    Service: {
      Type: 'simple',
      Restart: 'on-failure',
      EnvironmentFile: '-/etc/default/teleport',
      ExecStart: "/usr/local/bin/teleport start --pid-file=/run/teleport.pid --diag-addr=http://127.0.0.1:3434 --config=#{teleport_config}",
      ExecReload: '/bin/kill -HUP $MAINPID',
      PIDFile: '/run/teleport.pid',
      LimitNOFILE: '8192'
    },
    Install: {
      WantedBy: 'multi-user.target'
    }
  )
  action :create
  notifies :restart, 'service[teleport]', :immediately
end

# Teleport is started in its package postinst
service 'teleport' do
  supports status: true
  action [:enable, :start]
end

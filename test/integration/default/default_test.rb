# InSpec tests for recipe gitlab-teleport::default

control 'general-checks' do
  impact 1.0
  title 'General tests for gitlab-teleport cookbook'
  desc '
    This control ensures that:
      * there is no duplicates in /etc/group'

  describe etc_group do
    its('gids') { should_not contain_duplicates }
  end
end

control 'general-teleport-checks' do
  impact 1.0
  title 'General tests for Teleport service'
  desc '
    This control ensures that:
      * teleport package is installed
      * teleport service in enabled and running'

  describe package('teleport-ent') do
    it { should be_installed }
  end

  # describe service('teleport') do
  #   it { should be_installed }
  #   it { should be_enabled }
  #   it { should be_running }
  # end
end

control 'teleport-config-checks' do
  impact 1.0
  title 'Tests teleport settings'
  desc '
    This control ensures that:
      * correct ports are used
      * config files exist'

  # # Auth Server
  # describe port(3025) do
  #   its('processes') { should eq ['teleport'] }
  #   its('addresses') { should eq ['127.0.0.1'] }
  #   its('protocols') { should eq ['tcp'] }
  # end

  # # Tunnel Listener
  # describe port(3024) do
  #   its('processes') { should eq ['teleport'] }
  #   its('addresses') { should eq ['0.0.0.0'] }
  #   its('protocols') { should eq ['tcp'] }
  # end

  # # Proxy Service
  # describe port(3023) do
  #   its('processes') { should eq ['teleport'] }
  #   its('addresses') { should eq ['0.0.0.0'] }
  #   its('protocols') { should eq ['tcp'] }
  # end

  # # Web Interface
  # describe port(3080) do
  #   its('processes') { should eq ['teleport'] }
  #   its('addresses') { should eq ['0.0.0.0'] }
  #   its('protocols') { should eq ['tcp'] }
  # end

  describe file('/etc/teleport/config.yaml') do
    its('mode') { should cmp '0600' }
  end

  # describe file('/var/lib/teleport/host_uuid') do
  #   it { should exist }
  # end
  # describe file('/var/lib/teleport/license.pem') do
  #   it { should exist }
  # end
end

---- Drop users if they exist
DROP OWNED BY "console-ro";
DROP OWNED BY "console-rw";
DROP OWNED BY "console-admin";
DROP USER IF EXISTS "console-ro";
DROP USER IF EXISTS "console-rw";
DROP USER IF EXISTS "console-admin";

--- Teleport integration, admin user
CREATE USER "console-admin" WITH SUPERUSER CREATEDB CREATEROLE;

--- Teleport integration, Read Only user
CREATE USER "console-ro";
GRANT SELECT ON ALL TABLES IN SCHEMA public TO "console-ro";

--- Teleport integration, Read Write user
CREATE USER "console-rw";
GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE ON ALL TABLES IN SCHEMA public  TO "console-rw";

--- Set Default privileges for new tables
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT ON TABLES TO "console-ro";
ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE ON TABLES TO "console-rw";

--- For main and CI DB
ALTER DEFAULT PRIVILEGES FOR ROLE gitlab IN SCHEMA public GRANT SELECT ON TABLES TO "console-ro";
ALTER DEFAULT PRIVILEGES FOR ROLE gitlab IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE ON TABLES TO "console-rw";

--- For registry DB
ALTER DEFAULT PRIVILEGES FOR ROLE "gitlab-registry" IN SCHEMA public GRANT SELECT ON TABLES TO "console-ro";
ALTER DEFAULT PRIVILEGES FOR ROLE "gitlab-registry" IN SCHEMA public GRANT SELECT, INSERT, UPDATE, DELETE, TRUNCATE ON TABLES TO "console-rw";
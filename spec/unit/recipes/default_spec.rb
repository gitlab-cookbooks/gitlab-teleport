# Cookbook:: gitlab-teleport
# Spec:: default
#
# Copyright:: 2020, GitLab B.V., MIT.

require 'spec_helper'

describe 'gitlab-teleport::default' do
  context 'when all attributes are default, on Ubuntu 20.04' do
    before do
      mock_secrets_path = 'test/integration/data_bags/gitlab-teleport/gitlab-teleport.json'
      secrets = JSON.parse(File.read(mock_secrets_path))
      # Actual arg values don't matter, just need to match the node attribs in the let(:chef_run)
      expect_any_instance_of(Chef::Recipe).to receive(:get_secrets)
        .with('hashicorp_vault', 'secrets', 'key').and_return(secrets)
    end

    let(:chef_run) do
      ChefSpec::SoloRunner.new(
        platform: 'ubuntu',
        version: '20.04',
      ) do |node|
        node.normal['gitlab-teleport']['secrets'] = {
          'backend' => 'hashicorp_vault',
          'path' => 'secrets',
          'key' => 'key',
        }
      end.converge(described_recipe)
    end

    it 'converges succesfully' do
      expect { chef_run }.to_not raise_error
    end
  end
end
